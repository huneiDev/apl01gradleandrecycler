package io.matas.gradleandrecycler;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

/**
 * Created by Matas Ramasauskas on 17/12/2016.
 * For Mattdevelops LTC.
 * You can contact me at : dev.matas@gmail.com
 * Or see link for more details http://mattdevleops.github.io
 */

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_page_activity);

        String recievedName = getIntent().getExtras().getString("nameField");
        TextView textView = (TextView)findViewById(R.id.secondPageText);
        textView.setText(recievedName);
        Log.d("Tag","Gavau varda " + recievedName);
    }


}
